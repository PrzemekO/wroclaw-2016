import {Component, Input} from 'angular2/core';

@Component({

	selector: 'anothercomp',
    template: `


<h1 [hidden]="name!='Mariusz'">Mariusz is lecturing inC025</h1>
<h1 [hidden]="name!=pattern">
  {{notiffication}} 
</h1>


`
})

export class Anothercomp {
	@Input() name: string;
	@Input() pattern: string;
	@Input() notiffication: string;

}

