import {Component} from 'angular2/core';
import {Anothercomp} from './anothercomp';

@Component({

  // Declare the tag name in index.html to where the component attaches
  selector: 'hello-world',
  directives: [Anothercomp],
  template: `
 <div>
 <anothercomp [name]="yourName" [notiffication]="yourNotiffication" [pattern]="yourPattern"></anothercomp>
  </div>
    <label>Name:</label>
 <input type="text" [(ngModel)]="yourName" placeholder="Fill with name">

  <hr>
  <!-- conditionally display yourName --> 
<label>Choose your pattern:</label>

 <input type="text" [(ngModel)]="yourPattern" placeholder="insert your pattern">
 <hr>
 <label>Choose your notification:</label>
 <input type="text" [(ngModel)]="yourNotiffication" placeholder="insert your notification">
   
   <hr> 
 <h1 [hidden]="!yourName || yourName=='Mariusz'">Hello {{yourName}} nice to meet you!</h1> 
`
})

export class HelloWorld {
  // Declaring the variable for binding with initial value
   yourName: string = '';
   yourPattern: string = '';
   yourNotiffication: string = '';
  //@Output() myevent: EventEmitter = new EventEmitter<string>(); 
}
